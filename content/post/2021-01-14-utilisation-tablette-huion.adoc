---
title: "Configuration de la tablette Huion sous linux Ubuntu"
date: 2021-01-14
authors: ["Fred"]
draft: false
summary: Configurer la tablette Huion Kamvas Pro 13 en multi-écran sous Ubuntu
tags: ["tablette", "huion", "linux"]
---

= Configuration de la tablette Huion sous linux Ubuntu

:icons: image

La tablette Huion Kamvas Pro 13 est une alternative intéressante
à la Wacom One 13 ; elle possède des caractéristiques similaires,
un écran amincit, des boutons en plus, et surtout elle est moins chère que la Wacom !

Par contre, pas de drivers officiel Linux, ni de support natif sous Ubuntu.
Donc voyons comment la faire fonctionner sous Ubuntu.

== Installation du driver

Récupération de la dernière version des drivers DIGImend sur https://github.com/DIGImend/digimend-kernel-drivers/releases

Actuellement, c'est `digimend-dkms_10_all.deb`

Installation du package téléchargé sous Ubuntu (20.04/20.10)

[source,sh]
----
sudo apt-get install dkms
sudo dpkg -i digimend-dkms_10_all.deb
----

Puis redémarrage du système pour charger les nouveaux modules.


== Configuration multi-écran

Quand on branche la tablette, le stylet est par défaut calé sur l'écran principal ;
donc en mode miroir (copie de l'écran principal sur la tablette), cela fonctionne,
mais dès que l'on passe en mode écran étendu, en particulier avec un 3ème écran
(portable + écran externe + tablette), cela ne fonctionne plus.
Il faut recaler les coordonnées du stylet sur les coordonnées de l'écran de la tablette...

Il faut donc trouver l'entrée (input) correspondant au stylet,
trouver l'écran correspondant à l'écran de la tablette,
puis caler l'un sur l'autre.

=== Le stylet

Problème : l'installation des drivers semble donner des résultats différents suivant la version d'Ubuntu utilisée.
Par exemple, sur un portable Dell avec une Ubuntu 20.04, la commande `xinput` donne :
[source,sh]
----
⎜   ↳ Tablet Monitor Pad                      	id=17	[slave  pointer  (2)]
⎜   ↳ Tablet Monitor                          	id=18	[slave  pointer  (2)]
⎜   ↳ Tablet Monitor Dial                     	id=19	[slave  pointer  (2)]
⎜   ↳ Tablet Monitor Touch Strip pad          	id=21	[slave  pointer  (2)]
----

La bonne entrée, du stylet, est ici la "Tablet Monitor" d'id 18, il faut le deviner !!

De même, la commande `xsetwacom list` renvoie :
[source,sh]
----
Tablet Monitor Touch Strip pad  	id: 21	type: PAD
----
Pas de stylet (Pen) dans la liste !

Sur un autre ordinateur portable, installé en Ubuntu 20.10,
avec le même driver et la même configuration
(l'écran et la tablette sont connectées à une station d'accueil)

[source,sh]
----
⎜   ↳ Tablet Monitor stylus                   	id=11	[slave  pointer  (2)]
⎜   ↳ Tablet Monitor Pad pad                  	id=12	[slave  pointer  (2)]
⎜   ↳ Tablet Monitor Touch Strip pad          	id=13	[slave  pointer  (2)]
⎜   ↳ Tablet Monitor Dial                     	id=14	[slave  pointer  (2)]
----

La commande `xsetwacom list` renvoie :
[source,sh]
----
Tablet Monitor stylus           	id: 11	type: STYLUS
Tablet Monitor Pad pad          	id: 12	type: PAD
Tablet Monitor Touch Strip pad  	id: 13	type: PAD
----
Youpi, il y a un stylet !

=== L'écran

On utilise ici `xrandr` qui permet d'avoir la liste des écrans connectés :

[source,sh]
----
> xrandr |grep " connected"

eDP-1 connected primary 1920x1080+0+1200 (normal left inverted right x axis y axis) 309mm x 173mm
DP-1-1 connected 1920x1080+1920+1200 (normal left inverted right x axis y axis) 345mm x 195mm
DP-1-2 connected 1920x1200+1920+0 (normal left inverted right x axis y axis) 518mm x 324mm
----

En l'occurrence, ici, c'est le `DP-1-1` (à vérifier)


=== Calibrage

Avec les infos précédentes, sous Ubuntu 20.04,
il faut alors calibrer le stylet (id 18) sur l'écran de la tablette (DP-1-1) en utilisant `xinput` :
[source,sh]
----
xinput map-to-output 18 DP-1-1
----

[NOTE]
Truc amusant, l'id du stylet change à chaque déconnection/reconnection de la tablette branchée sur la station d'accueil via usb-c à l'ordinateur.

Sous Ubuntu 20.10, c'est encore plus simple, on peut passer par la commande `xsetwacom` car elle détecte le stylet, on peut donc utiliser son nom au lieu de son id :
[source,bash]
----
xsetwacom set "Tablet Monitor stylus" MapToOutput DP-1-1
----

[NOTE]
Cette commande ne change pas d'une connection à l'autre.

