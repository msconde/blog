---
title: Outils pour tablette graphique
date: 2020-10-22
tags: ["tablette", "openboard"]
authors: ["Nicolas","Anthony", "Jocelyn"]
draft: false
summary: Quelques outils indispensables pour utiliser une tablette graphique (wacom...) en cours et td 
---

# Outils pour tablette graphique WACOM ONE 13

Pour l'instant testées sur (K)Ubuntu `20.04`, reconnues immédiatement 
et configuration poussée possible par la configuration système. 

## Openboard [https://openboard.ch/](https://openboard.ch/)

Nicolas a construit une image modifiée à télécharger ici :
[https://gogit.univ-orleans.fr/lifo/no/openboard/releases](https://gogit.univ-orleans.fr/lifo/no/openboard/releases) qui facilite énormément l'utilisation des tablettes en tri-écran. 

Testé ce matin en salle E04 avec diffusion simultanée sur Teams
(qui n'a redémarré que trois fois pendant l'heure et demie de test, on n'est pas si mal).

```
chmod a+x OpenBoard-*.AppImage
```

Tableau blanc interactif, l'image fournie permet de lancer `Openboard` avec les contrôles sur l'écran principal, 
l'écran sans contrôles étant lui projeté en se plaçant en haut à droite de l'écran 
principal, avec un décalage de un pixel vers la droite. 

La partie principale considère donc la configuration des écrans : 
+ la tablette doit être l'écran principal (ou primaire) 
+ le vidéo-projecteur doit être l'écran situé directement à droite 
+ l'écran de l'ordinateur (placé ou bon vous semble) permet de laisser Teams ouvert pour voir les éventuelles questions (et surtout les éventuels bugs) 

Par exemple, sur une `Kubuntu 20.04` : 

![Configuration écrans](./configuration-ecrans.png)

La dernière étape consiste à configurer le stylet pour le calibrer au niveau de l'écran : la totalité de la tablette doit correspondre à l'image projetée. Sur `Kubuntu`, la fenêtre de configuration ci-dessous se trouve dans la *Configuration du système*, en tapant `Tablette graphique` dans la barre de recherche : `Matériel > Périphériques d'entrée > Tablette graphique`.  

![Configuration du stylet](./configuration-stylet.png)

Une fenêtre apparaît sur la tablette, il faut amener le curseur sur les quatre coins de la tablette pour la calibrer 
(cette étape demande une certaine dextérité). Ensuite, il ne reste qu'à lancer : 

```
./OpenBoard-*.AppImage
```

En configuration finale, on obtient donc `Openboard` :

+ avec les contrôles sur la tablette
+ sans les contrôles sur le vidéo-proj 

Et `Teams` reste visible sur l'écran de l'ordinateur. Ou plus visuel :

![Exemple de configuration](./configuration-tablette-bureau.jpg)

## Xournal [https://doc.ubuntu-fr.org/xournal](https://doc.ubuntu-fr.org/xournal)

Permet d'annoter des PDF, ou d'obtenir une feuille A4 (à carreaux) sur laquelle écrire : 

<img src="https://doc.ubuntu-fr.org/lib/exe/fetch.php?tok=502e16&media=https%3A%2F%2Fframalibre.org%2Fsites%2Fdefault%2Ffiles%2Fksnip_20200409-004701.png" width="500" alt="Xournal" />

## Tableau blanc interactif Teams

Accessible depuis n'importe quelle réunion (via le partage d'écran), semble bien fonctionner. 
Notons que la documentation se contredit elle-même sur l'utilisation une fois la réunion finie : 
c'est disponible sur iOS mais non disponible sur iOS !

![Documentation Teams](./teams-whiteboard.png)

[![C'est mes yeux ou quoi ?](./cestmesyeuxouquoi.png)](https://www.youtube.com/watch?v=sNu1rv7o0Zs)


# Utiliser un iPad comme tablette graphique


## Ingrédients 

- Spacedesk (gratuit, il me semble)
- iPad Air (moins gratuit, il me semble)
- Windows 10
- Apple Pencil


Note : Il s'agit des outils que j'ai utilisé, et il y a peut être moyen d'adapter à votre situation. En l'occurrence, il me semble que le seul "impératif" est d'être sur Windows 10; mais j'ai l'impression que ça marcherait avec une tablette android.


## Installation


- Télécharger et installer l'application serveur depuis le site https://spacedesk.net/ (NOTE IMPORTANTE : il semblerait qu'il y ait des problèmes avec les anciens drivers Nvidia pouvant causer des Blue Screen of Death. Voir la note sur le site sur ce sujet)
- Télécharger l'application Spacedesk depuis le Playstore ou l'App Store sur votre tablette
- Lancer l'application sur votre ordinateur
- Lancer l'application sur la tablette, normalement votre ordinateur devrait apparaitre dans la liste sur l'écran, cliquer dessus.
- Enjoy

A ce moment là, vous devriez voir l'écran de votre windows "recopié" sur votre tablette. Il ne reste plus qu'à lancer un logiciel de prise de note, par exemple Xournal++, et théoriquement vous pouvez écrire sur votre tablette à la main (ou avec un éventuel crayon adapté).

Pour ma part, je conseillerai plutôt d'utiliser  l'écran de votre tablette comme un écran secondaire; comme ça vous pouvez avoir votre écran principal avec Teams ouvert, ou des notes et Xournal++ sur votre tablette. Pour ce faire, sur windows allez dans Paramètres > Système > Affichage. Dans la section "Plusieurs affichages", selectionnez "étendre ces affichages". Normalement vous pourrez accéder à votre tablette en basculant la souris tout à droite ou tout à gauche de votre écran principal.


## Limites / Problèmes

Je n'ai pas pu faire de tests poussés, du coup je peux difficilement garantir que cela marche et avec quelle fiabilité. J'ai pu néanmoins remarquer quelques petites choses que je préfère mentionner :

- un peu de latence entre l'ordinateur et la tablette. Pas énormément mais ça se sent quand on dessine/écrit maniabilité (par exemple retour en arrière) à prendre en main 
- j'ai un peu peur de la non détection de paume pour ceux qui utiliserait un crayon pas fait pour ça (en tout cas avec mon Apple Pencil je n'ai pas eu de problème)
- j'ai tenté de faire la même manipulation depuis mon ordinateur fixe, mais ça ne marche pas (est-ce que ca vient du problème Nvidia (l'ordi sur lequel ça fonctionne est AMD ?), mais j'avoue ne pas avoir poussé plus loin
- je ne sais pas ce que ça vaut en pratique avec de vieilles tablettes (compatibilité? latence?..)


Si néanmoins ça marche chez vous, et que vous trouvez ça pratique, tant mieux ! Sachez néanmoins que l'expérience est (au moins) 50% plus satisfaisante avec une vraie tablette graphique, mais ça peut donner un bon aperçu !

