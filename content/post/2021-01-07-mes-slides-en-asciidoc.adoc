---
title: "Mes slides en asciidoc/revealJS/pdf en déploiement continu Gitlab !"
date: 2021-01-07
authors: ["Fred"]
draft: true
summary: Comment écrire ses slides en Asciidoc, les compiler en Reveal JS et les déployer automatiquement sous Gitlab
tags: ["asciidoc", "revealjs", "gitlab"]
---

= Mes slides en asciidoc/revealJS/pdf en déploiement continu Gitlab !

On continue l'utilisation amusante d'Asciidoc pour réaliser des supports de cours,
en les générant des slides Reveal JS et un joli poly en PDF :)


== Installation locale

Installation des packages nécessaires à la compilation *locale* sous Ubuntu 20.10

[source,sh]
----
apt-get install asciidoctor graphviz openjdk-15-jre

gem install asciidoctor-html5s
gem install asciidoctor-diagram
gem install asciidoctor-revealjs
gem install asciidoctor-pdf
----


== Configuration du projet

On commence par créer un répertoire dans lequel on va mettre créer l'arborescence du projet
[source,sh]
----
mkdir slides
cd slides
mkdir images
mkdir plugins
----

Les slides en html ont besoin de reveal.js ; on récupère donc Reveal.js, en version 3.9 car incompatibilité actuellement avec la version 4.0
[source,sh]
----
git clone -b 3.9.2 --depth 1 https://github.com/hakimel/reveal.js.git
----


Puis compilation des slides en local :
[source,sh]
----
asciidoctor-revealjs -r asciidoctor-diagram -r asciidoctor-html5s index.adoc
----
génère un `index.html`, qui nécessite Reveal.js dans un sous-répertoire.



== Contenu des slides

